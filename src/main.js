/* eslint-disable */ 
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import router from './router'

import VueFire from 'vuefire'
import firebase from 'firebase/app'
import 'firebase/firestore'

Vue.config.productionTip = false

Vue.use(VueFire)


firebase.initializeApp({
  apiKey: "AIzaSyAXhahEvbDCMeAG1OFSyJFxJyCjL0wkYLw",
    authDomain: "vuefire-45aad.firebaseapp.com",
    databaseURL: "https://vuefire-45aad.firebaseio.com",
    projectId: "vuefire-45aad",
    storageBucket: "vuefire-45aad.appspot.com",
    messagingSenderId: "877860713745"
})
export const db = firebase.firestore()
db.settings({timestampsInSnapshots: true});


/* eslint-disable no-new */
new Vue({
  // el: '#app',
  router,
  // components: { App },
  template: `
    <div id="app">
      <ul>
          <li>
              <router-link to='/'>App</router-link>
          </li>
          <li>
              <router-link to='/componentexample'>component</router-link>
          </li>
          <li>
              <router-link to='/register'>Register</router-link>
          </li>
      </ul>
      <router-view></router-view>
    </div>
  `
}).$mount('#app')
