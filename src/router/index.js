/* eslint-disable */ 

import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import componentexample from '@/components/componentexample'
import Register from '@/components/Register'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: __dirname,
  routes: [
    {path: '/', name: 'HelloWorld', component: HelloWorld},
    {path: '/componentexample', name: 'componentexample', component: componentexample},
    {path: '/Register', name: 'Register', component: Register},
  ]
})
